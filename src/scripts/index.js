import '../styles/index.scss';

const apiKey = '563492ad6f917000010000012886a77914c249e3ae891021ee4191f1';
const apiUrl = 'https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=Bread&per_page=15&page=1';


$(document).ready(() => {
    console.log('ready');
});

function renderPhotos(photos) {
    console.log(photos);
    $(".card-columns").empty();
    $.each(photos, (i, photo) => {
        $(".card-columns").append(createOtherCard(photo));
    });
}

function createOtherCard(photo) {
    return `
    <div class="card text-white mt-5 bg-white">
    <div class="card-header">${photo.id}</div>
    <div class="card-body">
    <img class="card-img-top rounded" src="${photo.src.medium}" alt="Card image cap">
        <h5 class="card-title">${photo.photographer}</h5>
        <p class="card-text">${photo.photographer_url}</p>
    </div>
</div>
    `;
}

var possibleAmounts = [1, 5, 10, 30];
possibleAmounts.forEach(number => {
    $(".dropdown-menu").append(
        `<button class="dropdown-item">${number}</button>`
    )
});

var chosenAmount = 5;

$(".dropdown-item").click(function () {
    chosenAmount = this.innerText;
    console.log(chosenAmount);
});

//current search term
var currentSearchTerm = '';

//searchInput
$("#searchInput").change(function () {
    currentSearchTerm = this.value;
    console.log(currentSearchTerm);
});

//searchButton
$("#searchButton").click(function () {
    $.ajax({
        url: `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${currentSearchTerm}&per_page=${chosenAmount}&page=1`,
        type: 'GET',
        headers: {
            'Authorization': `Bearer ${apiKey}`
        }
    }).done(data => {
        renderPhotos(data.photos);
    });
});



/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */
window.helloWorld = function () {
    console.log('HEllooOooOOo!');
};